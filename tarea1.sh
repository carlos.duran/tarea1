#!/bin/bash

timestamp=$(date +"%d-%m-%Y_%H-%M-%S")

echo "Script para realizar respaldos..."
read -p "Por favor ingrese la ruta a respaldar -> " ruta

filename=$(basename $ruta)
exitroute=$filename_$timestamp

if [ -f $ruta ]
then
  echo "La ruta ingresada es un archivo."
  echo "Comprimiendo archivo "$filename
  tar -czvf /tmp/$exitroute $filename >> /dev/null
  echo "Compresión exitosa del archivo."
fi

if [ -d $ruta ]
then
  echo "La ruta ingresada es un directorio."
  echo "Comprimiendo directorio" $ruta
  tar -czvf /tmp/$filename.tar.gz $ruta >> /dev/null
  echo "Compresión exitosa del directorio. "
fi