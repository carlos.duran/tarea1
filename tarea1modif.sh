#!/bin/bash

### Variables
entrada="$1"
fecha=$(date +%s)

### Exit codes
# 3 --> No es un archivo ni directorio
# return
# exit termina el script

function valida_archivo()
{
  # Valida si la entrada del script es una cadena vacìa o no fue ingresada
  if [ -z $entrada ]
  then
    echo "Debe escribir un nombre de archivo o carpeta..."
  # Valida si es un archivo regular y existe
  elif [ -f $entrada ]
  then
    echo "Es un archivo..."
    limpiar_ruta
    comprimir
  # Valida si es un directorio y existe
  elif [ -d $entrada ]
  then
    echo "Es un directorio..."
    limpiar_ruta
    comprimir
  else
  # Imprime codigo de salida
    echo "Error inesperado..."
    echo "El archivo no existe, no se pudo encontrar o hay un error de lectura en disco"
    exit 3
  fi
}

function limpiar_ruta()
{
  data=$(basename $entrada)
  echo $data
  respaldo=${data}_${fecha}.tar.gz
  echo $respaldo
}

function comprimir()
{
  destino="/tmp/"
  echo "Creando respaldo en /tmp "
  tar -czf ${destino}${respaldo} $entrada 2> /dev/null
  #Validar si hay codigo de erro en ejecucion de comando
  exitcode=$?
  if ! [ $exitcode -eq 0 ]
  then
    echo "ERROR: codigo de salida: $exitcode"
  fi
}

valida_archivo

